## The Goal

1. Please download the hour.csv from https://archive.ics.uci.edu/ml/machine-learning-databases/00275/

2. Please build an API (Flask/Django) with a Nginx/Gunicorn server in docker
   - Build one endpoint where you return the standard deviation in bike rentals (for registered and casual users separately) for every weekday (i.e. Monday, Tuesday, Wednesday, Thursday, Friday) (using parallel processing)
   - Please implement best practices TDD/BDD and https://www.python.org/dev/peps/pep-0008/

3. Please provide instructions on how the docker can be started and how the API will be accessed.


## How To

### Build docker image

make build

```
registry.gitlab.com/tonywangcn/backend-api-example   20190412193043      7d1ead111f3f        2 minutes ago       1.1GB
registry.gitlab.com/tonywangcn/backend-api-example   prod                7d1ead111f3f        2 minutes ago       1.1GB
```
When it finished, run "docker images" you willl see the images above successfully built.

### Test the API

make test

```
Attaching to django-app
django-app | System check identified no issues (0 silenced).
django-app | ..
django-app | ----------------------------------------------------------------------
django-app | Ran 2 tests in 0.251s
django-app | 
django-app | OK
django-app exited with code 0
```

Successful log

### Run it in production mode

make prod

```
curl http://localhost:8000/bike/stdweekday/

[{"1":{"casual":35.09705595297562,"registered":159.51789677007724}},{"3":{"casual":27.79065753221199,"registered":172.3447516018455}},{"4":{"casual":27.76808849271999,"registered":169.32739480916905}},{"2":{"casual":26.17089454469394,"registered":170.103245349532}},{"5":{"casual":36.48753369106089,"registered":149.90597714025927}}]
```