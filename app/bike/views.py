from rest_framework.response import Response
from rest_framework.views import APIView

import multiprocessing as mp
import pandas as pd


def process_func(df, weekday, output):
    """
    Caculate std of registered and casual users for every weekday, then put the result into Queue
    """
    results = {}

    for types in ["casual", "registered"]:
        std = df[df["weekday"] == weekday][types].std()
        results.update({types: std})

    output.put({"{}".format(weekday): results})


class SdByWeekdayAPIView(APIView):
    def get(self, request, format=None):
        """
        return a list of std results
        """
        data_dir = "/usr/src/app/data/" + "hour.csv"

        # read csv file into pandas
        df = pd.read_csv(data_dir)

        # create a Queue to put result of std
        output = mp.Queue()

        # Setup a list of process to run, 5 processer
        processes = [
            mp.Process(target=process_func, args=(df, weekday, output))
            for weekday in range(1, 6)
        ]
        # Run processes
        for p in processes:
            p.start()

        # Exit the completed processes
        for p in processes:
            p.join()

        results = [output.get() for _ in processes]

        return Response(results)

