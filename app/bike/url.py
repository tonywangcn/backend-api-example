from django.conf.urls import url, include

from .views import SdByWeekdayAPIView

from rest_framework import routers

router = routers.SimpleRouter(trailing_slash=False)

urlpatterns = [
    url(r"^stdweekday/$", SdByWeekdayAPIView.as_view(), name="getstdbyweekday")
]

urlpatterns += router.urls
