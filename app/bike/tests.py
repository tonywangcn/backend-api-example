from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

import pandas as pd


class BikeRentalStdTests(APITestCase):
    def setUp(self):
        self.data_dir = "/usr/src/app/data/" + "hour.csv"
        self.df = pd.read_csv(self.data_dir)

    def test_res_stats(self):
        url = reverse("getstdbyweekday")
        res = self.client.get(url)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_res_result(self):
        url = reverse("getstdbyweekday")
        res = self.client.get(url)
        json_data = res.data
        for weekday in json_data:
            for all_types in weekday.values():
                for types in all_types:
                    value = all_types[types]
                    std = self.df[self.df["weekday"] == int(list(weekday.keys())[0])][
                        types
                    ].std()
                    self.assertEqual(value, std)

