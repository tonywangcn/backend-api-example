Django==1.11.7
django-cors-headers==2.1.0
djangorestframework==3.7.3
gunicorn==19.9.0
pandas==0.24.2