SRV_NAME=backend-api-example
REPO_EX=registry.gitlab.com
NAME_SPACE=tonywangcn
REPO=${REPO_EX}
TAG=$(shell date +%Y%m%d%H%M%S)
FIXTAG?=prod
NAME=${REPO}/${NAME_SPACE}/${SRV_NAME}

build:
	echo build ${NAME}:${TAG}
	cp docker/base/Dockerfile .
	docker build -t ${NAME}:${FIXTAG} .
	docker tag ${NAME}:${FIXTAG} ${NAME}:${TAG}
	rm Dockerfile

dev:
	docker-compose up -d --force-recreate

prod:
	docker-compose -f docker-compose-prod.yml up -d --force-recreate

test:
	docker-compose -f docker-compose-test.yml up --exit-code-from django --force-recreate

down:
	docker-compose down